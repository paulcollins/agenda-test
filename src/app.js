const Agenda = require('agenda');

var mongoConnectionString = 'mongodb://127.0.0.1/agenda';

var agenda = new Agenda({ db: { address: mongoConnectionString } });

// or override the default collection name:
// var agenda = new Agenda({db: {address: mongoConnectionString, collection: 'jobCollectionName'}});

// or pass additional connection options:
// var agenda = new Agenda({db: {address: mongoConnectionString, collection: 'jobCollectionName', options: {ssl: true}}});

// or pass in an existing mongodb-native MongoClient instance
// var agenda = new Agenda({mongo: myMongoClient});

agenda.define('trigger power fail', function(job, done) {
    console.log('doing it');
    done();
});

agenda.on('ready', function() {
    //agenda.every('10 seconds', 'delete old users');
    agenda.schedule('in 1 hour', 'trigger power fail');


    agenda.start();
});